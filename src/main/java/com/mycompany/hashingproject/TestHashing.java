/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashingproject;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class TestHashing {

    public static void main(String[] args) {
        Hash<String, Integer> h = new Hash<String, Integer>();
        Scanner kb = new Scanner(System.in);
        String a1 = kb.next();

        while (!a1.equals("*")) {
            int a2 = kb.nextInt();
            h.put(a1, a2);
            a1 = kb.next();
        }
        a1 = kb.next();

        if (h.get(a1) == null) {
            System.out.println(h.remove(a1));
            System.out.println("Remove success");
        } else {
            System.out.println(h.get(a1));
        }
    }
}
