/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashingproject;

/**
 *
 * @author ACER
 */
public class Hash<Key, Value> {

    private int map = 100;
    private Value[] vals = (Value[]) new Object[map];
    private Key[] keys = (Key[]) new Object[map];

    private int hash(Key key) {
        return (key.hashCode() & 0X7FFFFFFF) % map;
    }

    public Value get(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }

    public void put(Key key, Value val) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                keys[i] = key;
                vals[i] = val;
                break;
            }
            i = (i + 1) % map;
        }
        keys[i] = key;
        vals[i] = val;
    }

    public int remove(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                int temp = (int)keys[i];
                keys[i] = null;
                return temp;
            }
            i = (i + 1) % map;
        }
        return 0;
    }
}
